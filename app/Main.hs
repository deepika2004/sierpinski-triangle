module Main where
import System.Random
import Graphics.Gloss

vertices :: [(Float, Float)]
vertices = [(0, 0),(150, 0),(75,129.903)]


centroid :: (Float, Float)
centroid = (sumX / numVertices, sumY / numVertices)
  where
    numVertices = fromIntegral (length vertices)
    sumX = sum $ map fst vertices
    sumY = sum $ map snd vertices

main :: IO ()
main = do
    display (InWindow "Chaos Triangle" (800, 600) (0,0)) white picture


repeatVertices :: [(Float, Float)]
repeatVertices = take 30000 $ cycle vertices


picture :: Picture
picture = pictures [pointsAsPicture $ points1 centroid 1]

point :: (Float, Float) -> Picture
point (x, y) = translate x y $ circleSolid (0.5)

nextVertex :: (Float, Float) -> (Float, Float) -> (Float, Float)
nextVertex (x1, y1) (x2, y2) = ((x1 +x2) / 2, (y1 +y2) / 2)

points1 :: (Float,Float) -> Int -> [(Float, Float)]
points1 _ 30000 = []
points1 p i  =  nxtv : points1 nxtv (i+1)
        where nxtv = nextVertex (randomVertex repeatVertices i) p




randomValue :: [a] -> StdGen -> a
randomValue xs gen =
  let (index, _) = randomR (0, length xs - 1) gen
  in xs !! index

randomVertex::[a] ->Int ->  a
randomVertex v i = randomValue v (mkStdGen i)

pointsAsPicture :: [(Float, Float)] -> Picture
pointsAsPicture = pictures . map point
